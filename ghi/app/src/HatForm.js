import React, { useEffect, useState } from 'react';


export default function HatForm() {
  const [locations, setLocations] = useState([])
  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color: '',
    picture_url: '',
    location: '',
  })


  const fetchLocationData = async () => {
    const location_url = 'http://localhost:8100/api/locations/';
    const response = await fetch(location_url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchLocationData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8090/api/hats/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: '',
      });
    }
  }


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Fabric" value={formData.fabric} required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Style_name" value={formData.style_name} required type="text" name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style name</label>
            </div>

             <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" value={formData.color} name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
               <input onChange={handleFormChange} placeholder="Picture_url" required type="text" value={formData.picture_url} name="picture_url" id="picture_url" className="form-control" />
               <label htmlFor="picture_url">Picture URL</label>
            </div>

             <div className="mb-3">
               <select onChange={handleFormChange} required name="location" id="location" value={formData.location} className="form-select">
                 <option value="">Choose a location</option>
                 {locations.map((location, index) => {
                  return (
                    <option key={index} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
