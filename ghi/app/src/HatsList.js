import { useEffect, useState } from "react";


export default function HatsList() {
 const [hats, setHats] = useState([])
  const fetchData = async () => {
    const hat_url = 'http://localhost:8090/api/hats/';
    const response = await fetch(hat_url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }
  useEffect(() => {
    fetchData();
  }, []);



  const handleDelete = async (hat) =>{

    const url = `http://localhost:8090/api/hats/${hat.id}/`;

    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);  //   const newHatList =hats.filter((hat) => hat !== deletingHat);
    if (response.ok) {
      fetchData();
    }
  }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>
                  <img
                  src={hat.picture_url}
                  alt={hat.style_name + " pic"}
                  width={100}
                  height={100}
                  />
                </td>
                <td>{hat.location.closet_name}</td><td><button onClick={() => handleDelete(hat)}>Delete</button></td>
              </tr>
          );
        })}
      </tbody>
    </table>
  );
}
