import { useEffect, useState } from "react";


export default function ShoeList() {
  const [shoes, setShoes] = useState([])
    const fetchData = async () => {
      const shoe_url = 'http://localhost:8080/api/shoes/';
      const response = await fetch(shoe_url);
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      }
    }
    useEffect(() => {
      fetchData();
    }, []);


    const handleDelete = async (shoe) =>{

      const url = `http://localhost:8080/api/shoes/${shoe.id}/`;

      const fetchConfig = {
        method: "delete",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        fetchData();
      }
    }


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img
                  src={shoe.picture_url}
                  alt={shoe.model_name + " pic"}
                  width={100}
                  height={100}
                  />
                </td>
                <td>{shoe.bins.closet_name}</td><td><button onClick={() => handleDelete(shoe)}>Delete</button></td>
              </tr>
            ) ;
          })}
        </tbody>
      </table>
    );
  }
