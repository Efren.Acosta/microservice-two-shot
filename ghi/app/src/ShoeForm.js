import React, { useEffect, useState } from "react";


export default function ShoeForm() {
  const [bins, setBins] = useState([]);
  const [formData, setFormData] = useState({
    manufacturer: "",
    model_name: "",
    color: "",
    picture_url: "",
    bin: "",
  });


  const fetchBinData = async () => {
    const bin_url = "http://localhost:8100/api/bins/";
    const response = await fetch(bin_url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };
  useEffect(() => {
    fetchBinData();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8080/api/shoes/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: "",
      });
    }
  };


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Manufacturer"
                value={formData.manufacturer}
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Model_name"
                value={formData.model_name}
                required
                type="text"
                name="model_name"
                id="model_name"
                className="form-control"
              />
              <label htmlFor="model_name">Model name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Color"
                required
                type="text"
                value={formData.color}
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Picture_url"
                required
                type="text"
                value={formData.picture_url}
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="bin"
                id="bin"
                value={formData.bin}
                className="form-select"
              >
                <option value="">Choose a bin</option>
                {bins.map((bin, index) => {
                  return (
                    <option key={index} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
