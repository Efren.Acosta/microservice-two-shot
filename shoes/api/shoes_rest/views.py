from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVoEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bins",
        "id",
    ]

    encoders = {
        "bins": BinVoEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            import_href = content["bins"]
            bins = BinVO.objects.get(import_href=import_href)
            content["bins"] = bins
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400,
            )
    shoes = Shoe.objects.create(**content)
    return JsonResponse(
        shoes,
        encoder=ShoeDetailEncoder,
        safe=False
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse(
                {"message": "Shoe does not exist"},
                status=400
            )
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400,
                )
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = [ "manufacturer", "model_name", "color", "picture_url", "bins", ]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400,
                )
